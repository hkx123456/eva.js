import * as PIXI_2 from '@tbminiapp/pixi-miniprogram-engine';

declare const atob_2: any;
export { atob_2 as atob }

declare const cancelAnimationFrame_2: any;
export { cancelAnimationFrame_2 as cancelAnimationFrame }

declare const _default: typeof PIXI_2.miniprogram;
export default _default;

declare const devicePixelRatio_2: any;
export { devicePixelRatio_2 as devicePixelRatio }

export declare const dispatchMouseDown: any;

export declare const dispatchMouseMove: any;

export declare const dispatchMouseUp: any;

export declare const dispatchPointerDown: any;

export declare const dispatchPointerMove: any;

export declare const dispatchPointerUp: any;

export declare const dispatchTouchEnd: any;

export declare const dispatchTouchMove: any;

export declare const dispatchTouchStart: any;

export declare const documentAlias: any;

declare const Element_2: any;
export { Element_2 as Element }

declare const Event_2: any;
export { Event_2 as Event }

declare const EventTarget_2: any;
export { EventTarget_2 as EventTarget }

export declare const globalAlias: any;

declare const HTMLCanvasElement_2: any;
export { HTMLCanvasElement_2 as HTMLCanvasElement }

declare const HTMLElement_2: any;
export { HTMLElement_2 as HTMLElement }

declare const HTMLImageElement_2: any;
export { HTMLImageElement_2 as HTMLImageElement }

declare const HTMLMediaElement_2: any;
export { HTMLMediaElement_2 as HTMLMediaElement }

declare const HTMLVideoElement_2: any;
export { HTMLVideoElement_2 as HTMLVideoElement }

declare const Image_2: any;
export { Image_2 as Image }

declare const navigator_2: any;
export { navigator_2 as navigator }

declare const Node_2: any;
export { Node_2 as Node }

declare const performance_2: any;
export { performance_2 as performance }

export declare const registerCanvas: any;

export declare const registerCanvas2D: any;

declare const requestAnimationFrame_2: any;
export { requestAnimationFrame_2 as requestAnimationFrame }

declare const screen_2: any;
export { screen_2 as screen }

export declare const windowAlias: any;

export declare const XMLHttpRequestAlias: any;

export { }
